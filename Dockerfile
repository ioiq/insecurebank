FROM jackielou-docker.jfrog.io/tomcat:latest

ADD contrast.jar /usr/local/tomcat/lib
COPY *.war /usr/local/tomcat/webapps/

EXPOSE 8080
CMD ["catalina.sh", "run"]
